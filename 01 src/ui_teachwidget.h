/********************************************************************************
** Form generated from reading UI file 'teachwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEACHWIDGET_H
#define UI_TEACHWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TeachWidget
{
public:
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_2;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *run;
    QPushButton *clearAllButton;
    QVBoxLayout *verticalLayout;
    QPushButton *moveJ;
    QPushButton *moveLine;
    QPushButton *moveArc;
    QPushButton *deletePathButton;
    QCheckBox *checkBox;

    void setupUi(QWidget *TeachWidget)
    {
        if (TeachWidget->objectName().isEmpty())
            TeachWidget->setObjectName(QStringLiteral("TeachWidget"));
        TeachWidget->resize(404, 307);
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TeachWidget->sizePolicy().hasHeightForWidth());
        TeachWidget->setSizePolicy(sizePolicy);
        TeachWidget->setMinimumSize(QSize(0, 0));
        verticalLayout_3 = new QVBoxLayout(TeachWidget);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBox = new QGroupBox(TeachWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        groupBox->setMinimumSize(QSize(0, 0));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        listWidget = new QListWidget(groupBox);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setMinimumSize(QSize(0, 0));

        verticalLayout_2->addWidget(listWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        run = new QPushButton(groupBox);
        run->setObjectName(QStringLiteral("run"));

        horizontalLayout->addWidget(run);

        clearAllButton = new QPushButton(groupBox);
        clearAllButton->setObjectName(QStringLiteral("clearAllButton"));

        horizontalLayout->addWidget(clearAllButton);


        verticalLayout_2->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        moveJ = new QPushButton(groupBox);
        moveJ->setObjectName(QStringLiteral("moveJ"));

        verticalLayout->addWidget(moveJ);

        moveLine = new QPushButton(groupBox);
        moveLine->setObjectName(QStringLiteral("moveLine"));

        verticalLayout->addWidget(moveLine);

        moveArc = new QPushButton(groupBox);
        moveArc->setObjectName(QStringLiteral("moveArc"));

        verticalLayout->addWidget(moveArc);

        deletePathButton = new QPushButton(groupBox);
        deletePathButton->setObjectName(QStringLiteral("deletePathButton"));

        verticalLayout->addWidget(deletePathButton);

        checkBox = new QCheckBox(groupBox);
        checkBox->setObjectName(QStringLiteral("checkBox"));

        verticalLayout->addWidget(checkBox);


        horizontalLayout_2->addLayout(verticalLayout);


        verticalLayout_4->addLayout(horizontalLayout_2);


        verticalLayout_3->addWidget(groupBox);


        retranslateUi(TeachWidget);

        QMetaObject::connectSlotsByName(TeachWidget);
    } // setupUi

    void retranslateUi(QWidget *TeachWidget)
    {
        TeachWidget->setWindowTitle(QApplication::translate("TeachWidget", "Form", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("TeachWidget", "\347\244\272\346\225\231\347\225\214\351\235\242", Q_NULLPTR));
        run->setText(QApplication::translate("TeachWidget", "\350\277\220\350\241\214", Q_NULLPTR));
        clearAllButton->setText(QApplication::translate("TeachWidget", "\346\270\205\347\251\272\345\205\250\351\203\250\350\275\250\350\277\271", Q_NULLPTR));
        moveJ->setText(QApplication::translate("TeachWidget", "\347\251\272\347\247\273\347\233\264\347\272\277", Q_NULLPTR));
        moveLine->setText(QApplication::translate("TeachWidget", "\347\233\264\347\272\277", Q_NULLPTR));
        moveArc->setText(QApplication::translate("TeachWidget", "\345\234\206\345\274\247", Q_NULLPTR));
        deletePathButton->setText(QApplication::translate("TeachWidget", "\345\210\240\351\231\244\350\275\250\350\277\271", Q_NULLPTR));
        checkBox->setText(QApplication::translate("TeachWidget", "\350\277\220\345\212\250\351\230\264\345\275\261", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class TeachWidget: public Ui_TeachWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEACHWIDGET_H
