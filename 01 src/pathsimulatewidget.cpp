﻿#include "pathsimulatewidget.h"
#include "drawpathwidget.h"
#include <QtWidgets>
#include "pathsimulatecontrolwidget.h"


PathSimulateWidget::PathSimulateWidget(QWidget *parent) : QWidget(parent),
    currentText(""),
    drawPathWidget(QSharedPointer<DrawPathWidget>(new DrawPathWidget)),
    textEdit(QSharedPointer<QTextEdit>(new QTextEdit)),
    openBnt(QSharedPointer<QPushButton>(new QPushButton)),
    gridLayout(QSharedPointer<QGridLayout>(new QGridLayout)),
    controlWidget(QSharedPointer<PathSimulateControlWidget>(new PathSimulateControlWidget(this)))
{
    init();
    layout();
    createAction();
}


void PathSimulateWidget::init()
{
    openBnt->setText(QStringLiteral("打开"));

    drawPathWidget->setAutoFillBackground(true);
    QPalette palette = drawPathWidget->palette();
    palette.setColor(QPalette::Window, Qt::black);
    drawPathWidget->setPalette(palette);

    controlWidget->setAutoFillBackground(true);
    QPalette controlWidgetPalette = controlWidget->palette();
    controlWidgetPalette.setColor(QPalette::Window, Qt::white);
    controlWidget->setPalette(controlWidgetPalette);

}


void PathSimulateWidget::layout()
{
    gridLayout->addWidget(drawPathWidget.data(),0, 0, 1, 1);
    gridLayout->addWidget(controlWidget.data(),0, 1, 1, 1);
    gridLayout->addWidget(textEdit.data(), 1, 0, 1, 1);

    gridLayout->setRowStretch(0, 4);
    gridLayout->setRowStretch(1, 1);
    gridLayout->setColumnStretch(0, 4);
    gridLayout->setColumnStretch(1, 1);

    gridLayout->setHorizontalSpacing(5);
    gridLayout->setVerticalSpacing(5);
    gridLayout->setContentsMargins(5, 5, 5, 5);
    setLayout(gridLayout.data());
}


void PathSimulateWidget::createAction()
{
    connect(this->controlWidget.data(), SIGNAL(loadButtonSignal()), this, SLOT(loadPath()));
    connect(this->controlWidget.data(), SIGNAL(deleteButtonSignal()), this, SLOT(deletePath()));
    connect(this->controlWidget.data(), SIGNAL(currentTextSignal(QString)), this, SLOT(setCurrentText(QString)));
    connect(this->controlWidget.data(), SIGNAL(computerButtonSignal(QVector<double>)), this, SLOT(computer(QVector<double>)));

}

/***************槽函数************************/
void PathSimulateWidget::loadPath()
{
    QString fileName = QFileDialog::getOpenFileName(this, QStringLiteral("打开"), "./", QStringLiteral("轨迹数据(*.txt *.TXT)"));
    if (!fileName.isEmpty()) {
        displayOnTextEdit(QStringLiteral("轨迹加载中..."));
        this->drawPathWidget->open(fileName);
        this->controlWidget->addItem(fileName);
        displayOnTextEdit(QStringLiteral("轨迹加载完成:") + fileName);
    } else {
        displayOnTextEdit(QStringLiteral("请选择轨迹文件"));
    }
}

void PathSimulateWidget::deletePath()
{
    if (!this->currentText.isEmpty()) {
        displayOnTextEdit(QStringLiteral("删除轨迹文件:") + currentText);
        this->drawPathWidget->close(currentText);
        this->controlWidget->deleteItem();
    } else {
        displayOnTextEdit(QStringLiteral("请选择要删除的轨迹文件"));
    }
    this->currentText = "";
}


/* 在textEdit上显示内容
 * @info 要显示的信息字符串
 **/
void PathSimulateWidget::displayOnTextEdit(const QString& info) const
{
    static int index = 0;
    index++;
    QString str = QString("[%1] %2").arg(index).arg(info);
    this->textEdit->append(str);
}


void PathSimulateWidget::setCurrentText(QString currentText)
{
    this->currentText = currentText;
}


/** 计算相交点槽函数
 * @intersectorInfo 求交面信息数组
 */
void PathSimulateWidget::computer(QVector<double> intersectorInfo)
{
    if (intersectorInfo.size() != 9) return;

    osg::Vec3d startPoint(intersectorInfo[0], intersectorInfo[1], intersectorInfo[2]);
    osg::Vec3d endPoint(intersectorInfo[3], intersectorInfo[4], intersectorInfo[5]);
    osg::Vec3d upVector(intersectorInfo[6], intersectorInfo[7], intersectorInfo[8]);

    /// 根据求交面获取相交点数组
    QVector<osg::Vec3d> intersectiorPoints = this->drawPathWidget->getIntersectorPoint(startPoint, endPoint, upVector);

    if (!intersectiorPoints.empty()) {

        displayOnTextEdit(QString(QStringLiteral("检测到相交点个数为: %1")).arg(intersectiorPoints.size()));

        for (int i = 0; i < intersectiorPoints.size(); ++i) {

            osg::Vec3d& point = intersectiorPoints[i];

            displayOnTextEdit(QString(QStringLiteral("相交点%1坐标: (%2, %3, %4)")).arg(i+1).arg(point.x()).arg(point.y()).arg(point.z()));

        }

    } else {

        displayOnTextEdit(QStringLiteral("没有检测到相交点"));

    }

}


