﻿#include "customobjectdialog.h"
#include "ui_customobjectdialog.h"

CustomObjectDialog::CustomObjectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CustomObjectDialog)
{
    ui->setupUi(this);
    init();
}


CustomObjectDialog::~CustomObjectDialog()
{
    delete ui;
}


void CustomObjectDialog::init()
{

    ui->longLineEdit->setText("100");
    ui->widthLineEdit->setText("100");
    ui->heightLineEdit->setText("100");

    ui->radiusLineEdit->setText("30");

    ui->cylinderRadiusLineEdit->setText("100");
    ui->cylinderHeightLineEdit->setText("100");


}


void CustomObjectDialog::on_okButton_clicked()
{
    float length = ui->longLineEdit->text().toFloat();
    float width = ui->widthLineEdit->text().toFloat();
    float height = ui->heightLineEdit->text().toFloat();
    emit buildBox({length, width, height});
    accept();
}

void CustomObjectDialog::on_cancelButton_clicked()
{
    close();
}


void CustomObjectDialog::on_okButton_2_clicked()
{
    float radius = ui->radiusLineEdit->text().toFloat();
    emit buildBall(radius);
    accept();
}


void CustomObjectDialog::on_cancelButton_2_clicked()
{
    close();
}


void CustomObjectDialog::on_okButton_3_clicked()
{
    float radius = ui->cylinderRadiusLineEdit->text().toFloat();
    float height = ui->cylinderHeightLineEdit->text().toFloat();
    emit buildCylinder({radius, height});
    accept();
}

void CustomObjectDialog::on_cancelButton_3_clicked()
{
    close();
}
