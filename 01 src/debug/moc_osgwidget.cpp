/****************************************************************************
** Meta object code from reading C++ file 'osgwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../osgwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'osgwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OsgWidget_t {
    QByteArrayData data[17];
    char stringdata0[209];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OsgWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OsgWidget_t qt_meta_stringdata_OsgWidget = {
    {
QT_MOC_LITERAL(0, 0, 9), // "OsgWidget"
QT_MOC_LITERAL(1, 10, 14), // "isMoveFinished"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 13), // "runNewThread1"
QT_MOC_LITERAL(4, 40, 13), // "runNewThread2"
QT_MOC_LITERAL(5, 54, 13), // "runNewThread3"
QT_MOC_LITERAL(6, 68, 13), // "runNewThread4"
QT_MOC_LITERAL(7, 82, 13), // "runNewThread5"
QT_MOC_LITERAL(8, 96, 12), // "readFinished"
QT_MOC_LITERAL(9, 109, 19), // "setCurrentJointText"
QT_MOC_LITERAL(10, 129, 4), // "text"
QT_MOC_LITERAL(11, 134, 20), // "setCurrentObjectText"
QT_MOC_LITERAL(12, 155, 12), // "moveAllJoint"
QT_MOC_LITERAL(13, 168, 13), // "drawOneShadow"
QT_MOC_LITERAL(14, 182, 8), // "recvFile"
QT_MOC_LITERAL(15, 191, 8), // "NodeInfo"
QT_MOC_LITERAL(16, 200, 8) // "loadFile"

    },
    "OsgWidget\0isMoveFinished\0\0runNewThread1\0"
    "runNewThread2\0runNewThread3\0runNewThread4\0"
    "runNewThread5\0readFinished\0"
    "setCurrentJointText\0text\0setCurrentObjectText\0"
    "moveAllJoint\0drawOneShadow\0recvFile\0"
    "NodeInfo\0loadFile"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OsgWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       7,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    1,   80,    2, 0x06 /* Public */,
       4,    1,   83,    2, 0x06 /* Public */,
       5,    1,   86,    2, 0x06 /* Public */,
       6,    1,   89,    2, 0x06 /* Public */,
       7,    1,   92,    2, 0x06 /* Public */,
       8,    0,   95,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    1,   96,    2, 0x0a /* Public */,
      11,    1,   99,    2, 0x0a /* Public */,
      12,    0,  102,    2, 0x0a /* Public */,
      13,    0,  103,    2, 0x0a /* Public */,
      14,    1,  104,    2, 0x0a /* Public */,
      16,    0,  107,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 15,    2,
    QMetaType::Void,

       0        // eod
};

void OsgWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        OsgWidget *_t = static_cast<OsgWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->isMoveFinished(); break;
        case 1: _t->runNewThread1((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->runNewThread2((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->runNewThread3((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->runNewThread4((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->runNewThread5((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->readFinished(); break;
        case 7: _t->setCurrentJointText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->setCurrentObjectText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->moveAllJoint(); break;
        case 10: _t->drawOneShadow(); break;
        case 11: _t->recvFile((*reinterpret_cast< NodeInfo(*)>(_a[1]))); break;
        case 12: _t->loadFile(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (OsgWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OsgWidget::isMoveFinished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (OsgWidget::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OsgWidget::runNewThread1)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (OsgWidget::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OsgWidget::runNewThread2)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (OsgWidget::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OsgWidget::runNewThread3)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (OsgWidget::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OsgWidget::runNewThread4)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (OsgWidget::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OsgWidget::runNewThread5)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (OsgWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OsgWidget::readFinished)) {
                *result = 6;
                return;
            }
        }
    }
}

const QMetaObject OsgWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_OsgWidget.data,
      qt_meta_data_OsgWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *OsgWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OsgWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OsgWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int OsgWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void OsgWidget::isMoveFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void OsgWidget::runNewThread1(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void OsgWidget::runNewThread2(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void OsgWidget::runNewThread3(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void OsgWidget::runNewThread4(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void OsgWidget::runNewThread5(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void OsgWidget::readFinished()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
