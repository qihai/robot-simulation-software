/****************************************************************************
** Meta object code from reading C++ file 'pathsimulatecontrolwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../pathsimulatecontrolwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'pathsimulatecontrolwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_PathSimulateControlWidget_t {
    QByteArrayData data[12];
    char stringdata0[229];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_PathSimulateControlWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_PathSimulateControlWidget_t qt_meta_stringdata_PathSimulateControlWidget = {
    {
QT_MOC_LITERAL(0, 0, 25), // "PathSimulateControlWidget"
QT_MOC_LITERAL(1, 26, 16), // "loadButtonSignal"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 18), // "deleteButtonSignal"
QT_MOC_LITERAL(4, 63, 20), // "computerButtonSignal"
QT_MOC_LITERAL(5, 84, 15), // "QVector<double>"
QT_MOC_LITERAL(6, 100, 17), // "currentTextSignal"
QT_MOC_LITERAL(7, 118, 21), // "on_loadButton_clicked"
QT_MOC_LITERAL(8, 140, 23), // "on_deleteButton_clicked"
QT_MOC_LITERAL(9, 164, 32), // "on_listWidget_currentTextChanged"
QT_MOC_LITERAL(10, 197, 11), // "currentText"
QT_MOC_LITERAL(11, 209, 19) // "on_computer_clicked"

    },
    "PathSimulateControlWidget\0loadButtonSignal\0"
    "\0deleteButtonSignal\0computerButtonSignal\0"
    "QVector<double>\0currentTextSignal\0"
    "on_loadButton_clicked\0on_deleteButton_clicked\0"
    "on_listWidget_currentTextChanged\0"
    "currentText\0on_computer_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_PathSimulateControlWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,
       4,    1,   56,    2, 0x06 /* Public */,
       6,    1,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       7,    0,   62,    2, 0x08 /* Private */,
       8,    0,   63,    2, 0x08 /* Private */,
       9,    1,   64,    2, 0x08 /* Private */,
      11,    0,   67,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    2,
    QMetaType::Void, QMetaType::QString,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   10,
    QMetaType::Void,

       0        // eod
};

void PathSimulateControlWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        PathSimulateControlWidget *_t = static_cast<PathSimulateControlWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->loadButtonSignal(); break;
        case 1: _t->deleteButtonSignal(); break;
        case 2: _t->computerButtonSignal((*reinterpret_cast< QVector<double>(*)>(_a[1]))); break;
        case 3: _t->currentTextSignal((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->on_loadButton_clicked(); break;
        case 5: _t->on_deleteButton_clicked(); break;
        case 6: _t->on_listWidget_currentTextChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->on_computer_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QVector<double> >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (PathSimulateControlWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PathSimulateControlWidget::loadButtonSignal)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (PathSimulateControlWidget::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PathSimulateControlWidget::deleteButtonSignal)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (PathSimulateControlWidget::*_t)(QVector<double> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PathSimulateControlWidget::computerButtonSignal)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (PathSimulateControlWidget::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&PathSimulateControlWidget::currentTextSignal)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject PathSimulateControlWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_PathSimulateControlWidget.data,
      qt_meta_data_PathSimulateControlWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *PathSimulateControlWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *PathSimulateControlWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_PathSimulateControlWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int PathSimulateControlWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void PathSimulateControlWidget::loadButtonSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void PathSimulateControlWidget::deleteButtonSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void PathSimulateControlWidget::computerButtonSignal(QVector<double> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void PathSimulateControlWidget::currentTextSignal(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
