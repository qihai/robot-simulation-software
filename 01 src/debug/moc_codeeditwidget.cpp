/****************************************************************************
** Meta object code from reading C++ file 'codeeditwidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.9)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../codeeditwidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'codeeditwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.9. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CodeEditWidget_t {
    QByteArrayData data[10];
    char stringdata0[115];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CodeEditWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CodeEditWidget_t qt_meta_stringdata_CodeEditWidget = {
    {
QT_MOC_LITERAL(0, 0, 14), // "CodeEditWidget"
QT_MOC_LITERAL(1, 15, 14), // "sendElemVector"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 17), // "QVector<Element*>"
QT_MOC_LITERAL(4, 49, 20), // "parseCodeAndSendText"
QT_MOC_LITERAL(5, 70, 7), // "newFile"
QT_MOC_LITERAL(6, 78, 4), // "open"
QT_MOC_LITERAL(7, 83, 4), // "save"
QT_MOC_LITERAL(8, 88, 6), // "saveAs"
QT_MOC_LITERAL(9, 95, 19) // "documentWasModified"

    },
    "CodeEditWidget\0sendElemVector\0\0"
    "QVector<Element*>\0parseCodeAndSendText\0"
    "newFile\0open\0save\0saveAs\0documentWasModified"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CodeEditWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   52,    2, 0x0a /* Public */,
       5,    0,   53,    2, 0x0a /* Public */,
       6,    0,   54,    2, 0x0a /* Public */,
       7,    0,   55,    2, 0x0a /* Public */,
       8,    0,   56,    2, 0x0a /* Public */,
       9,    0,   57,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool,
    QMetaType::Bool,
    QMetaType::Void,

       0        // eod
};

void CodeEditWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CodeEditWidget *_t = static_cast<CodeEditWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendElemVector((*reinterpret_cast< QVector<Element*>(*)>(_a[1]))); break;
        case 1: _t->parseCodeAndSendText(); break;
        case 2: _t->newFile(); break;
        case 3: _t->open(); break;
        case 4: { bool _r = _t->save();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 5: { bool _r = _t->saveAs();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: _t->documentWasModified(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (CodeEditWidget::*_t)(QVector<Element*> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&CodeEditWidget::sendElemVector)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject CodeEditWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CodeEditWidget.data,
      qt_meta_data_CodeEditWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CodeEditWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CodeEditWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CodeEditWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int CodeEditWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void CodeEditWidget::sendElemVector(QVector<Element*> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
