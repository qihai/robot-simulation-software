﻿#ifndef EVENTINTERACTION_H
#define EVENTINTERACTION_H
#include <osgGA/GUIEventHandler>    // osgGA::GUIEventHandler


#include <osgViewer/Viewer>
#include "global.h" // Joint

class SimulateControlWidget;

class EventInteraction: public osgGA::GUIEventHandler
{
    public:

        EventInteraction();

    public:

        // 重载handle函数
        virtual bool handle(const osgGA::GUIEventAdapter& guiEventAdapter, osgGA::GUIActionAdapter& guiActionAdapter);

        void pick(float x, float y);

        void pickJoint(float x, float y);

        void pickAxis(float x, float y);

        osg::Vec3 screen2World(float x, float y);

    private:

        void axisTranslate(const osgGA::GUIEventAdapter& guiEventAdapter);

        /// 绕xyz旋转
        void rotateBySelf(const osgGA::GUIEventAdapter& guiEventAdapter);

        /// 旋转关节
        void rotateJoint(const osgGA::GUIEventAdapter& guiEventAdapter);

        /// 获取偏移量
        osg::Vec3d getOffset(const JointType& joint);

        /// 关节绕自身的旋转轴旋转
        void rotateBySelf(const JointType& joint, osg::Matrix& curMatrix, const osg::Vec3d& offset, const float& angle);

    private:

        osg::ref_ptr<osgViewer::Viewer> pViewer;
        osg::ref_ptr<osg::MatrixTransform> picked;
        Axis axis;
        JointType joint;
        Circle circle;
        osg::Vec3 lastWorldPoint;

        SimulateControlWidget* simulateControlWidget;

};

#endif // EVENTINTERACTION_H
