/********************************************************************************
** Form generated from reading UI file 'setrobotparameterdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETROBOTPARAMETERDIALOG_H
#define UI_SETROBOTPARAMETERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SetRobotParameterDialog
{
public:
    QTabWidget *tabWidget;
    QWidget *robotParameter;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_20;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *d1;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *a1;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLineEdit *a2;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QLineEdit *a3;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QLineEdit *d4;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_19;
    QLineEdit *theta1LineEdit;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_20;
    QLineEdit *theta2LineEdit;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_21;
    QLineEdit *theta3LineEdit;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_22;
    QLineEdit *theta4LineEdit;
    QHBoxLayout *horizontalLayout_18;
    QLabel *label_23;
    QLineEdit *theta5LineEdit;
    QHBoxLayout *horizontalLayout_19;
    QLabel *label_24;
    QLineEdit *theta6LineEdit;
    QWidget *range;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_6;
    QLineEdit *J1_MIN;
    QLabel *label_7;
    QLineEdit *J1_MAX;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_8;
    QLineEdit *J2_MIN;
    QLabel *label_9;
    QLineEdit *J2_MAX;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_10;
    QLineEdit *J3_MIN;
    QLabel *label_11;
    QLineEdit *J3_MAX;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_14;
    QLineEdit *J4_MIN;
    QLabel *label_15;
    QLineEdit *J4_MAX;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_12;
    QLineEdit *J5_MIN;
    QLabel *label_13;
    QLineEdit *J5_MAX;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_17;
    QLineEdit *J6_MIN;
    QLabel *label_18;
    QLineEdit *J6_MAX;
    QWidget *tcf;
    QWidget *widget;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_16;
    QLineEdit *TCF_X_LineEdit;
    QHBoxLayout *horizontalLayout_21;
    QLabel *label_25;
    QLineEdit *TCF_Y_LineEdit;
    QHBoxLayout *horizontalLayout_22;
    QLabel *label_26;
    QLineEdit *TCF_Z_LineEdit;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QDialog *SetRobotParameterDialog)
    {
        if (SetRobotParameterDialog->objectName().isEmpty())
            SetRobotParameterDialog->setObjectName(QStringLiteral("SetRobotParameterDialog"));
        SetRobotParameterDialog->resize(400, 300);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SetRobotParameterDialog->sizePolicy().hasHeightForWidth());
        SetRobotParameterDialog->setSizePolicy(sizePolicy);
        tabWidget = new QTabWidget(SetRobotParameterDialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(20, 10, 361, 241));
        robotParameter = new QWidget();
        robotParameter->setObjectName(QStringLiteral("robotParameter"));
        layoutWidget = new QWidget(robotParameter);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(22, 12, 311, 191));
        horizontalLayout_20 = new QHBoxLayout(layoutWidget);
        horizontalLayout_20->setObjectName(QStringLiteral("horizontalLayout_20"));
        horizontalLayout_20->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        d1 = new QLineEdit(layoutWidget);
        d1->setObjectName(QStringLiteral("d1"));

        horizontalLayout->addWidget(d1);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_2->addWidget(label_2);

        a1 = new QLineEdit(layoutWidget);
        a1->setObjectName(QStringLiteral("a1"));

        horizontalLayout_2->addWidget(a1);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_3->addWidget(label_3);

        a2 = new QLineEdit(layoutWidget);
        a2->setObjectName(QStringLiteral("a2"));

        horizontalLayout_3->addWidget(a2);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        a3 = new QLineEdit(layoutWidget);
        a3->setObjectName(QStringLiteral("a3"));

        horizontalLayout_4->addWidget(a3);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        horizontalLayout_5->addWidget(label_5);

        d4 = new QLineEdit(layoutWidget);
        d4->setObjectName(QStringLiteral("d4"));

        horizontalLayout_5->addWidget(d4);


        verticalLayout_2->addLayout(horizontalLayout_5);


        horizontalLayout_20->addLayout(verticalLayout_2);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        label_19 = new QLabel(layoutWidget);
        label_19->setObjectName(QStringLiteral("label_19"));

        horizontalLayout_14->addWidget(label_19);

        theta1LineEdit = new QLineEdit(layoutWidget);
        theta1LineEdit->setObjectName(QStringLiteral("theta1LineEdit"));

        horizontalLayout_14->addWidget(theta1LineEdit);


        verticalLayout_3->addLayout(horizontalLayout_14);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        label_20 = new QLabel(layoutWidget);
        label_20->setObjectName(QStringLiteral("label_20"));

        horizontalLayout_15->addWidget(label_20);

        theta2LineEdit = new QLineEdit(layoutWidget);
        theta2LineEdit->setObjectName(QStringLiteral("theta2LineEdit"));

        horizontalLayout_15->addWidget(theta2LineEdit);


        verticalLayout_3->addLayout(horizontalLayout_15);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        label_21 = new QLabel(layoutWidget);
        label_21->setObjectName(QStringLiteral("label_21"));

        horizontalLayout_16->addWidget(label_21);

        theta3LineEdit = new QLineEdit(layoutWidget);
        theta3LineEdit->setObjectName(QStringLiteral("theta3LineEdit"));

        horizontalLayout_16->addWidget(theta3LineEdit);


        verticalLayout_3->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        label_22 = new QLabel(layoutWidget);
        label_22->setObjectName(QStringLiteral("label_22"));

        horizontalLayout_17->addWidget(label_22);

        theta4LineEdit = new QLineEdit(layoutWidget);
        theta4LineEdit->setObjectName(QStringLiteral("theta4LineEdit"));

        horizontalLayout_17->addWidget(theta4LineEdit);


        verticalLayout_3->addLayout(horizontalLayout_17);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        label_23 = new QLabel(layoutWidget);
        label_23->setObjectName(QStringLiteral("label_23"));

        horizontalLayout_18->addWidget(label_23);

        theta5LineEdit = new QLineEdit(layoutWidget);
        theta5LineEdit->setObjectName(QStringLiteral("theta5LineEdit"));

        horizontalLayout_18->addWidget(theta5LineEdit);


        verticalLayout_3->addLayout(horizontalLayout_18);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QStringLiteral("horizontalLayout_19"));
        label_24 = new QLabel(layoutWidget);
        label_24->setObjectName(QStringLiteral("label_24"));

        horizontalLayout_19->addWidget(label_24);

        theta6LineEdit = new QLineEdit(layoutWidget);
        theta6LineEdit->setObjectName(QStringLiteral("theta6LineEdit"));

        horizontalLayout_19->addWidget(theta6LineEdit);


        verticalLayout_3->addLayout(horizontalLayout_19);


        horizontalLayout_20->addLayout(verticalLayout_3);

        tabWidget->addTab(robotParameter, QString());
        range = new QWidget();
        range->setObjectName(QStringLiteral("range"));
        layoutWidget1 = new QWidget(range);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(30, 10, 259, 176));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(1);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(-1, -1, 0, -1);
        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_7->addWidget(label_6);

        J1_MIN = new QLineEdit(layoutWidget1);
        J1_MIN->setObjectName(QStringLiteral("J1_MIN"));

        horizontalLayout_7->addWidget(J1_MIN);

        label_7 = new QLabel(layoutWidget1);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setTextFormat(Qt::AutoText);
        label_7->setAlignment(Qt::AlignCenter);
        label_7->setWordWrap(false);

        horizontalLayout_7->addWidget(label_7);

        J1_MAX = new QLineEdit(layoutWidget1);
        J1_MAX->setObjectName(QStringLiteral("J1_MAX"));

        horizontalLayout_7->addWidget(J1_MAX);

        horizontalLayout_7->setStretch(0, 1);
        horizontalLayout_7->setStretch(1, 3);
        horizontalLayout_7->setStretch(2, 1);
        horizontalLayout_7->setStretch(3, 3);

        verticalLayout->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(1);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(-1, -1, 0, -1);
        label_8 = new QLabel(layoutWidget1);
        label_8->setObjectName(QStringLiteral("label_8"));

        horizontalLayout_8->addWidget(label_8);

        J2_MIN = new QLineEdit(layoutWidget1);
        J2_MIN->setObjectName(QStringLiteral("J2_MIN"));

        horizontalLayout_8->addWidget(J2_MIN);

        label_9 = new QLabel(layoutWidget1);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setTextFormat(Qt::AutoText);
        label_9->setAlignment(Qt::AlignCenter);
        label_9->setWordWrap(false);

        horizontalLayout_8->addWidget(label_9);

        J2_MAX = new QLineEdit(layoutWidget1);
        J2_MAX->setObjectName(QStringLiteral("J2_MAX"));

        horizontalLayout_8->addWidget(J2_MAX);

        horizontalLayout_8->setStretch(0, 1);
        horizontalLayout_8->setStretch(1, 3);
        horizontalLayout_8->setStretch(2, 1);
        horizontalLayout_8->setStretch(3, 3);

        verticalLayout->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(1);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(-1, -1, 0, -1);
        label_10 = new QLabel(layoutWidget1);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_9->addWidget(label_10);

        J3_MIN = new QLineEdit(layoutWidget1);
        J3_MIN->setObjectName(QStringLiteral("J3_MIN"));

        horizontalLayout_9->addWidget(J3_MIN);

        label_11 = new QLabel(layoutWidget1);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setTextFormat(Qt::AutoText);
        label_11->setAlignment(Qt::AlignCenter);
        label_11->setWordWrap(false);

        horizontalLayout_9->addWidget(label_11);

        J3_MAX = new QLineEdit(layoutWidget1);
        J3_MAX->setObjectName(QStringLiteral("J3_MAX"));

        horizontalLayout_9->addWidget(J3_MAX);

        horizontalLayout_9->setStretch(0, 1);
        horizontalLayout_9->setStretch(1, 3);
        horizontalLayout_9->setStretch(2, 1);
        horizontalLayout_9->setStretch(3, 3);

        verticalLayout->addLayout(horizontalLayout_9);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(1);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(-1, -1, 0, -1);
        label_14 = new QLabel(layoutWidget1);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_11->addWidget(label_14);

        J4_MIN = new QLineEdit(layoutWidget1);
        J4_MIN->setObjectName(QStringLiteral("J4_MIN"));

        horizontalLayout_11->addWidget(J4_MIN);

        label_15 = new QLabel(layoutWidget1);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setTextFormat(Qt::AutoText);
        label_15->setAlignment(Qt::AlignCenter);
        label_15->setWordWrap(false);

        horizontalLayout_11->addWidget(label_15);

        J4_MAX = new QLineEdit(layoutWidget1);
        J4_MAX->setObjectName(QStringLiteral("J4_MAX"));

        horizontalLayout_11->addWidget(J4_MAX);

        horizontalLayout_11->setStretch(0, 1);
        horizontalLayout_11->setStretch(1, 3);
        horizontalLayout_11->setStretch(2, 1);
        horizontalLayout_11->setStretch(3, 3);

        verticalLayout->addLayout(horizontalLayout_11);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(1);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(-1, -1, 0, -1);
        label_12 = new QLabel(layoutWidget1);
        label_12->setObjectName(QStringLiteral("label_12"));

        horizontalLayout_10->addWidget(label_12);

        J5_MIN = new QLineEdit(layoutWidget1);
        J5_MIN->setObjectName(QStringLiteral("J5_MIN"));

        horizontalLayout_10->addWidget(J5_MIN);

        label_13 = new QLabel(layoutWidget1);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setTextFormat(Qt::AutoText);
        label_13->setAlignment(Qt::AlignCenter);
        label_13->setWordWrap(false);

        horizontalLayout_10->addWidget(label_13);

        J5_MAX = new QLineEdit(layoutWidget1);
        J5_MAX->setObjectName(QStringLiteral("J5_MAX"));

        horizontalLayout_10->addWidget(J5_MAX);

        horizontalLayout_10->setStretch(0, 1);
        horizontalLayout_10->setStretch(1, 3);
        horizontalLayout_10->setStretch(2, 1);
        horizontalLayout_10->setStretch(3, 3);

        verticalLayout->addLayout(horizontalLayout_10);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(1);
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        horizontalLayout_13->setContentsMargins(-1, -1, 0, -1);
        label_17 = new QLabel(layoutWidget1);
        label_17->setObjectName(QStringLiteral("label_17"));

        horizontalLayout_13->addWidget(label_17);

        J6_MIN = new QLineEdit(layoutWidget1);
        J6_MIN->setObjectName(QStringLiteral("J6_MIN"));

        horizontalLayout_13->addWidget(J6_MIN);

        label_18 = new QLabel(layoutWidget1);
        label_18->setObjectName(QStringLiteral("label_18"));
        label_18->setTextFormat(Qt::AutoText);
        label_18->setAlignment(Qt::AlignCenter);
        label_18->setWordWrap(false);

        horizontalLayout_13->addWidget(label_18);

        J6_MAX = new QLineEdit(layoutWidget1);
        J6_MAX->setObjectName(QStringLiteral("J6_MAX"));

        horizontalLayout_13->addWidget(J6_MAX);

        horizontalLayout_13->setStretch(0, 1);
        horizontalLayout_13->setStretch(1, 3);
        horizontalLayout_13->setStretch(2, 1);
        horizontalLayout_13->setStretch(3, 3);

        verticalLayout->addLayout(horizontalLayout_13);

        tabWidget->addTab(range, QString());
        tcf = new QWidget();
        tcf->setObjectName(QStringLiteral("tcf"));
        widget = new QWidget(tcf);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(70, 50, 221, 121));
        verticalLayout_4 = new QVBoxLayout(widget);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        label_16 = new QLabel(widget);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_12->addWidget(label_16);

        TCF_X_LineEdit = new QLineEdit(widget);
        TCF_X_LineEdit->setObjectName(QStringLiteral("TCF_X_LineEdit"));

        horizontalLayout_12->addWidget(TCF_X_LineEdit);


        verticalLayout_4->addLayout(horizontalLayout_12);

        horizontalLayout_21 = new QHBoxLayout();
        horizontalLayout_21->setObjectName(QStringLiteral("horizontalLayout_21"));
        label_25 = new QLabel(widget);
        label_25->setObjectName(QStringLiteral("label_25"));

        horizontalLayout_21->addWidget(label_25);

        TCF_Y_LineEdit = new QLineEdit(widget);
        TCF_Y_LineEdit->setObjectName(QStringLiteral("TCF_Y_LineEdit"));

        horizontalLayout_21->addWidget(TCF_Y_LineEdit);


        verticalLayout_4->addLayout(horizontalLayout_21);

        horizontalLayout_22 = new QHBoxLayout();
        horizontalLayout_22->setObjectName(QStringLiteral("horizontalLayout_22"));
        label_26 = new QLabel(widget);
        label_26->setObjectName(QStringLiteral("label_26"));

        horizontalLayout_22->addWidget(label_26);

        TCF_Z_LineEdit = new QLineEdit(widget);
        TCF_Z_LineEdit->setObjectName(QStringLiteral("TCF_Z_LineEdit"));

        horizontalLayout_22->addWidget(TCF_Z_LineEdit);


        verticalLayout_4->addLayout(horizontalLayout_22);

        tabWidget->addTab(tcf, QString());
        layoutWidget2 = new QWidget(SetRobotParameterDialog);
        layoutWidget2->setObjectName(QStringLiteral("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(210, 260, 168, 22));
        horizontalLayout_6 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(layoutWidget2);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_6->addWidget(pushButton);

        pushButton_2 = new QPushButton(layoutWidget2);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout_6->addWidget(pushButton_2);


        retranslateUi(SetRobotParameterDialog);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(SetRobotParameterDialog);
    } // setupUi

    void retranslateUi(QDialog *SetRobotParameterDialog)
    {
        SetRobotParameterDialog->setWindowTitle(QApplication::translate("SetRobotParameterDialog", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("SetRobotParameterDialog", "d1:", Q_NULLPTR));
        label_2->setText(QApplication::translate("SetRobotParameterDialog", "a1:", Q_NULLPTR));
        label_3->setText(QApplication::translate("SetRobotParameterDialog", "a2:", Q_NULLPTR));
        label_4->setText(QApplication::translate("SetRobotParameterDialog", "a3:", Q_NULLPTR));
        label_5->setText(QApplication::translate("SetRobotParameterDialog", "d4:", Q_NULLPTR));
        label_19->setText(QApplication::translate("SetRobotParameterDialog", "\316\2701:", Q_NULLPTR));
        label_20->setText(QApplication::translate("SetRobotParameterDialog", "\316\2702:", Q_NULLPTR));
        label_21->setText(QApplication::translate("SetRobotParameterDialog", "\316\2703:", Q_NULLPTR));
        label_22->setText(QApplication::translate("SetRobotParameterDialog", "\316\2704:", Q_NULLPTR));
        label_23->setText(QApplication::translate("SetRobotParameterDialog", "\316\2705:", Q_NULLPTR));
        label_24->setText(QApplication::translate("SetRobotParameterDialog", "\316\2706", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(robotParameter), QApplication::translate("SetRobotParameterDialog", "MDH\345\217\202\346\225\260", Q_NULLPTR));
        label_6->setText(QApplication::translate("SetRobotParameterDialog", "J1:", Q_NULLPTR));
        label_7->setText(QApplication::translate("SetRobotParameterDialog", "---", Q_NULLPTR));
        label_8->setText(QApplication::translate("SetRobotParameterDialog", "J2:", Q_NULLPTR));
        label_9->setText(QApplication::translate("SetRobotParameterDialog", "---", Q_NULLPTR));
        label_10->setText(QApplication::translate("SetRobotParameterDialog", "J3:", Q_NULLPTR));
        label_11->setText(QApplication::translate("SetRobotParameterDialog", "---", Q_NULLPTR));
        label_14->setText(QApplication::translate("SetRobotParameterDialog", "J4:", Q_NULLPTR));
        label_15->setText(QApplication::translate("SetRobotParameterDialog", "---", Q_NULLPTR));
        label_12->setText(QApplication::translate("SetRobotParameterDialog", "J5:", Q_NULLPTR));
        label_13->setText(QApplication::translate("SetRobotParameterDialog", "---", Q_NULLPTR));
        label_17->setText(QApplication::translate("SetRobotParameterDialog", "J6:", Q_NULLPTR));
        label_18->setText(QApplication::translate("SetRobotParameterDialog", "---", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(range), QApplication::translate("SetRobotParameterDialog", "\345\205\263\350\212\202\350\277\220\345\212\250\350\214\203\345\233\264", Q_NULLPTR));
        label_16->setText(QApplication::translate("SetRobotParameterDialog", "X;", Q_NULLPTR));
        label_25->setText(QApplication::translate("SetRobotParameterDialog", "Y:", Q_NULLPTR));
        label_26->setText(QApplication::translate("SetRobotParameterDialog", "Z:", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tcf), QApplication::translate("SetRobotParameterDialog", "TCF\345\267\245\345\205\267\345\235\220\346\240\207\347\263\273", Q_NULLPTR));
        pushButton->setText(QApplication::translate("SetRobotParameterDialog", "OK", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("SetRobotParameterDialog", "Cancel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class SetRobotParameterDialog: public Ui_SetRobotParameterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETROBOTPARAMETERDIALOG_H
