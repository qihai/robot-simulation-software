/********************************************************************************
** Form generated from reading UI file 'customobjectdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CUSTOMOBJECTDIALOG_H
#define UI_CUSTOMOBJECTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CustomObjectDialog
{
public:
    QHBoxLayout *horizontalLayout;
    QTabWidget *tabWidget;
    QWidget *ranctangle;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_5;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QWidget *widget1;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_2;
    QLabel *longLable;
    QLineEdit *longLineEdit;
    QHBoxLayout *horizontalLayout_3;
    QLabel *widthLable;
    QLineEdit *widthLineEdit;
    QHBoxLayout *horizontalLayout_4;
    QLabel *heightLable;
    QLineEdit *heightLineEdit;
    QWidget *circle;
    QWidget *widget2;
    QHBoxLayout *horizontalLayout_6;
    QLabel *radiusLabel;
    QLineEdit *radiusLineEdit;
    QWidget *widget3;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *okButton_2;
    QPushButton *cancelButton_2;
    QWidget *cylinder;
    QWidget *widget4;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *okButton_3;
    QPushButton *cancelButton_3;
    QWidget *widget5;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label;
    QLineEdit *cylinderRadiusLineEdit;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_2;
    QLineEdit *cylinderHeightLineEdit;

    void setupUi(QDialog *CustomObjectDialog)
    {
        if (CustomObjectDialog->objectName().isEmpty())
            CustomObjectDialog->setObjectName(QStringLiteral("CustomObjectDialog"));
        CustomObjectDialog->resize(400, 300);
        horizontalLayout = new QHBoxLayout(CustomObjectDialog);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        tabWidget = new QTabWidget(CustomObjectDialog);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        ranctangle = new QWidget();
        ranctangle->setObjectName(QStringLiteral("ranctangle"));
        widget = new QWidget(ranctangle);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(180, 220, 168, 22));
        horizontalLayout_5 = new QHBoxLayout(widget);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
        okButton = new QPushButton(widget);
        okButton->setObjectName(QStringLiteral("okButton"));

        horizontalLayout_5->addWidget(okButton);

        cancelButton = new QPushButton(widget);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));

        horizontalLayout_5->addWidget(cancelButton);

        widget1 = new QWidget(ranctangle);
        widget1->setObjectName(QStringLiteral("widget1"));
        widget1->setGeometry(QRect(70, 30, 241, 161));
        verticalLayout = new QVBoxLayout(widget1);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        longLable = new QLabel(widget1);
        longLable->setObjectName(QStringLiteral("longLable"));

        horizontalLayout_2->addWidget(longLable);

        longLineEdit = new QLineEdit(widget1);
        longLineEdit->setObjectName(QStringLiteral("longLineEdit"));

        horizontalLayout_2->addWidget(longLineEdit);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        widthLable = new QLabel(widget1);
        widthLable->setObjectName(QStringLiteral("widthLable"));

        horizontalLayout_3->addWidget(widthLable);

        widthLineEdit = new QLineEdit(widget1);
        widthLineEdit->setObjectName(QStringLiteral("widthLineEdit"));

        horizontalLayout_3->addWidget(widthLineEdit);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        heightLable = new QLabel(widget1);
        heightLable->setObjectName(QStringLiteral("heightLable"));

        horizontalLayout_4->addWidget(heightLable);

        heightLineEdit = new QLineEdit(widget1);
        heightLineEdit->setObjectName(QStringLiteral("heightLineEdit"));

        horizontalLayout_4->addWidget(heightLineEdit);


        verticalLayout->addLayout(horizontalLayout_4);

        tabWidget->addTab(ranctangle, QString());
        circle = new QWidget();
        circle->setObjectName(QStringLiteral("circle"));
        widget2 = new QWidget(circle);
        widget2->setObjectName(QStringLiteral("widget2"));
        widget2->setGeometry(QRect(70, 70, 170, 24));
        horizontalLayout_6 = new QHBoxLayout(widget2);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        radiusLabel = new QLabel(widget2);
        radiusLabel->setObjectName(QStringLiteral("radiusLabel"));

        horizontalLayout_6->addWidget(radiusLabel);

        radiusLineEdit = new QLineEdit(widget2);
        radiusLineEdit->setObjectName(QStringLiteral("radiusLineEdit"));

        horizontalLayout_6->addWidget(radiusLineEdit);

        widget3 = new QWidget(circle);
        widget3->setObjectName(QStringLiteral("widget3"));
        widget3->setGeometry(QRect(180, 170, 168, 22));
        horizontalLayout_7 = new QHBoxLayout(widget3);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
        okButton_2 = new QPushButton(widget3);
        okButton_2->setObjectName(QStringLiteral("okButton_2"));

        horizontalLayout_7->addWidget(okButton_2);

        cancelButton_2 = new QPushButton(widget3);
        cancelButton_2->setObjectName(QStringLiteral("cancelButton_2"));

        horizontalLayout_7->addWidget(cancelButton_2);

        tabWidget->addTab(circle, QString());
        cylinder = new QWidget();
        cylinder->setObjectName(QStringLiteral("cylinder"));
        widget4 = new QWidget(cylinder);
        widget4->setObjectName(QStringLiteral("widget4"));
        widget4->setGeometry(QRect(180, 190, 168, 22));
        horizontalLayout_10 = new QHBoxLayout(widget4);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        okButton_3 = new QPushButton(widget4);
        okButton_3->setObjectName(QStringLiteral("okButton_3"));

        horizontalLayout_10->addWidget(okButton_3);

        cancelButton_3 = new QPushButton(widget4);
        cancelButton_3->setObjectName(QStringLiteral("cancelButton_3"));

        horizontalLayout_10->addWidget(cancelButton_3);

        widget5 = new QWidget(cylinder);
        widget5->setObjectName(QStringLiteral("widget5"));
        widget5->setGeometry(QRect(50, 50, 251, 91));
        verticalLayout_2 = new QVBoxLayout(widget5);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label = new QLabel(widget5);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_8->addWidget(label);

        cylinderRadiusLineEdit = new QLineEdit(widget5);
        cylinderRadiusLineEdit->setObjectName(QStringLiteral("cylinderRadiusLineEdit"));

        horizontalLayout_8->addWidget(cylinderRadiusLineEdit);


        verticalLayout_2->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_2 = new QLabel(widget5);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_9->addWidget(label_2);

        cylinderHeightLineEdit = new QLineEdit(widget5);
        cylinderHeightLineEdit->setObjectName(QStringLiteral("cylinderHeightLineEdit"));

        horizontalLayout_9->addWidget(cylinderHeightLineEdit);


        verticalLayout_2->addLayout(horizontalLayout_9);

        tabWidget->addTab(cylinder, QString());

        horizontalLayout->addWidget(tabWidget);


        retranslateUi(CustomObjectDialog);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(CustomObjectDialog);
    } // setupUi

    void retranslateUi(QDialog *CustomObjectDialog)
    {
        CustomObjectDialog->setWindowTitle(QApplication::translate("CustomObjectDialog", "Dialog", Q_NULLPTR));
        okButton->setText(QApplication::translate("CustomObjectDialog", "\347\224\237\346\210\220", Q_NULLPTR));
        cancelButton->setText(QApplication::translate("CustomObjectDialog", "\345\217\226\346\266\210", Q_NULLPTR));
        longLable->setText(QApplication::translate("CustomObjectDialog", "\351\225\277(mm):", Q_NULLPTR));
        widthLable->setText(QApplication::translate("CustomObjectDialog", "\345\256\275(mm):", Q_NULLPTR));
        heightLable->setText(QApplication::translate("CustomObjectDialog", "\351\253\230(mm):", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(ranctangle), QApplication::translate("CustomObjectDialog", "\347\237\251\345\275\242", Q_NULLPTR));
        radiusLabel->setText(QApplication::translate("CustomObjectDialog", "\345\215\212\345\276\204(mm):", Q_NULLPTR));
        okButton_2->setText(QApplication::translate("CustomObjectDialog", "\347\224\237\346\210\220", Q_NULLPTR));
        cancelButton_2->setText(QApplication::translate("CustomObjectDialog", "\345\217\226\346\266\210", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(circle), QApplication::translate("CustomObjectDialog", "\347\220\203\344\275\223", Q_NULLPTR));
        okButton_3->setText(QApplication::translate("CustomObjectDialog", "\347\224\237\346\210\220", Q_NULLPTR));
        cancelButton_3->setText(QApplication::translate("CustomObjectDialog", "\345\217\226\346\266\210", Q_NULLPTR));
        label->setText(QApplication::translate("CustomObjectDialog", "\345\215\212\345\276\204(mm):", Q_NULLPTR));
        label_2->setText(QApplication::translate("CustomObjectDialog", "\351\253\230\345\272\246(mm):", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(cylinder), QApplication::translate("CustomObjectDialog", "\345\234\206\346\237\261", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CustomObjectDialog: public Ui_CustomObjectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CUSTOMOBJECTDIALOG_H
