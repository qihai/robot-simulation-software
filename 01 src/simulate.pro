#-------------------------------------------------
#
# Project created by QtCreator 2021-01-20T14:52:34
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = simulate
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
        codeeditwidget.cpp \
        commonfunc.cpp \
        compensation.cpp \
        drawingarea.cpp \
        element.cpp \
        global.cpp \
        graphwidget.cpp \
        osgwidget.cpp \
        parsecode.cpp \
        robotsimulatewidget.cpp \
        simulatecontrolwidget.cpp \
        ifkinematicshandle.cpp \
        drawpathwidget.cpp \
        eventinteraction.cpp \
        pathsimulatewidget.cpp \
        setrobotparameterdialog.cpp \
        pathsimulatecontrolwidget.cpp \
        teachwidget.cpp \
        pathelement.cpp \
        readthread.cpp \
        customobjectdialog.cpp


HEADERS += \
        mainwindow.h \
        codeeditwidget.h \
        commonfunc.h \
        compensation.h \
        drawingarea.h \
        element.h \
        global.h \
        graphwidget.h \
        osgwidget.h \
        parsecode.h \
        robotsimulatewidget.h \
        simulatecontrolwidget.h \
        ifkinematicshandle.h \
        drawpathwidget.h \
        eventinteraction.h \
        pathsimulatewidget.h \
        setrobotparameterdialog.h \
        pathsimulatecontrolwidget.h \
        teachwidget.h \
        pathelement.h \
        readthread.h \
        customobjectdialog.h


FORMS += \
        mainwindow.ui \
        setrobotparameterdialog.ui \
        pathsimulatecontrolwidget.ui \
        teachwidget.ui \
        customobjectdialog.ui


libpath = $$PWD/osgLIB/build/lib/
win32: LIBS += -L$$libpath -lOpenThreadsd \
               -L$$libpath -losgd \
               -L$$libpath -losgDBd \
               -L$$libpath -losgGAd \
               -L$$libpath -losgQt5d \
               -L$$libpath -losgTextd \
               -L$$libpath -losgUtild \
               -L$$libpath -losgViewerd \


INCLUDEPATH += $$PWD/osgLIB/build/include
DEPENDPATH += $$PWD/osgLIB/build/include

QT += opengl

RESOURCES += \
    image.qrc


INCLUDEPATH += $$PWD/openLIB
INCLUDEPATH += $$PWD/openLIB/Eigen/src/LU



