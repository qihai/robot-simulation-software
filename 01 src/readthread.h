﻿#ifndef READTHREAD_H
#define READTHREAD_H

#include <QObject>
#include <osg/Node>
#include "global.h"

class ReadThread : public QObject
{

        Q_OBJECT

    public:

        explicit ReadThread(QObject *parent = nullptr);

    signals:

        void sendResult(NodeInfo);

    public slots:

        void readJoint(QString);

};

#endif // READTHREAD_H
