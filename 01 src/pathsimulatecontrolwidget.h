﻿#ifndef PATHSIMULATECONTROLWIDGET_H
#define PATHSIMULATECONTROLWIDGET_H

#include <QWidget>


namespace Ui {
class PathSimulateControlWidget;
}


class QPushButton;
class PathSimulateControlWidget : public QWidget
{
    Q_OBJECT

    public:

        PathSimulateControlWidget(QWidget *parent = 0);

        virtual ~PathSimulateControlWidget();

        void addItem(const QString& fileDir);

        void deleteItem();


    signals:

        void loadButtonSignal();

        void deleteButtonSignal();

        void computerButtonSignal(QVector<double>);

        void currentTextSignal(QString);

    private slots:

        void on_loadButton_clicked();

        void on_deleteButton_clicked();

        void on_listWidget_currentTextChanged(const QString &currentText);

        void on_computer_clicked();

    private:

        void init();

    private:

        Ui::PathSimulateControlWidget *ui;

};

#endif // PATHSIMULATECONTROLWIDGET_H
