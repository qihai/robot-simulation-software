#include "simulatecontrolwidget.h"
#include <QtWidgets>
#include "global.h"


SimulateControlWidget* SimulateControlWidget::instance = nullptr;


SimulateControlWidget::SimulateControlWidget(QWidget *parent) :
    QWidget(parent),
    removeJointButton(new QPushButton),
    removeObjectButton(new QPushButton),
    gridLayout(new QGridLayout),
    loadJointButton(new QPushButton),
    loadObjectButton(new QPushButton),
    jointListWidget(new QListWidget),
    objectListWidget(new QListWidget),
    jointListLabel(new QLabel),
    objectListLabel(new QLabel),
    setRobotParameterButton(new QPushButton),
    assembleToolButton(new QPushButton),
    customObjectButton(new QPushButton),
    hideAxis(new QRadioButton)
{
    init();
    layout();
    createAction();
}


SimulateControlWidget::~SimulateControlWidget()
{

}


void SimulateControlWidget::init()
{
    for (int i = 0; i < 7; ++i) {
        this->jointSlider.push_back(QSharedPointer<QSlider>(new QSlider));
        this->jointSpinBox.push_back(QSharedPointer<QSpinBox>(new QSpinBox));
        this->jointLabel.push_back(QSharedPointer<QLabel>(new QLabel));
        this->jointSlider.back()->setOrientation(Qt::Horizontal);
        this->jointSlider.back()->setMinimum(0);
        this->jointSlider.back()->setMaximum(360);
        this->jointSlider.back()->setSingleStep(STEP);
        this->jointSpinBox.back()->setMinimum(0);
        this->jointSpinBox.back()->setMaximum(360);
        this->jointSpinBox.back()->setSingleStep(STEP);
        this->jointLabel.back()->setText(QString(QStringLiteral("J%1")).arg(i));
    }

    loadJointButton->setText(QStringLiteral("加载关节"));
    loadObjectButton->setText(QStringLiteral("加载工件"));
    removeJointButton->setText(QStringLiteral("移除关节"));
    removeObjectButton->setText(QStringLiteral("移除工件"));


    setRobotParameterButton->setText(QStringLiteral("参数设置"));
    assembleToolButton->setText(QStringLiteral("装配工具"));
    customObjectButton->setText(QStringLiteral("自定义工件"));

    hideAxis->setText(QStringLiteral("显示坐标系"));
    jointListLabel->setText(QStringLiteral("关节列表"));
    objectListLabel->setText(QStringLiteral("工件列表"));

}


void SimulateControlWidget::layout()
{
    // row 0
    gridLayout->addWidget(jointListLabel.data(), 0, 0, 1, -1);
    // row 1
    gridLayout->addWidget(jointListWidget.data(), 1, 0, 1, -1);
    // row 2
    gridLayout->addWidget(objectListLabel.data(), 2, 0, 1, -1);
    // row 3
    gridLayout->addWidget(objectListWidget.data(), 3, 0, 1, -1);
    // row 4
    gridLayout->addWidget(loadJointButton.data(),         4, 0, 1, 1);
    gridLayout->addWidget(removeJointButton.data(),       4, 1, 1, 1);
    gridLayout->addWidget(loadObjectButton.data(),        4, 2, 1, 1);
    gridLayout->addWidget(removeObjectButton.data(),      4, 3, 1, 1);
    // row 5
    gridLayout->addWidget(hideAxis.data(),                  5, 0, 1, 1);
    gridLayout->addWidget(setRobotParameterButton.data(),   5, 1, 1, 1);
    gridLayout->addWidget(customObjectButton.data(),        5, 2, 1, 1);
    gridLayout->addWidget(assembleToolButton.data(),        5, 3, 1, 1);
    // row 6
    for (int i = 0; i < jointSlider.size(); ++i) {
        gridLayout->addWidget(jointLabel[i].data(),   6+i, 0, 1, 1);
        gridLayout->addWidget(jointSlider[i].data(),  6+i, 1, 1, 2);
        gridLayout->addWidget(jointSpinBox[i].data(), 6+i, 3, 1, 1);
    }

    gridLayout->setHorizontalSpacing(10);
    gridLayout->setVerticalSpacing(10);
    gridLayout->setContentsMargins(10, 10, 10, 10);
    setLayout(gridLayout.data());
}



QListWidget* SimulateControlWidget::getJointListWidet()
{
    return this->jointListWidget.data();
}


QListWidget* SimulateControlWidget::getObjectListWidet()
{
    return this->objectListWidget.data();
}


QRadioButton* SimulateControlWidget::getRadioButton()
{
    return this->hideAxis.data();
}



void SimulateControlWidget::insertJointItem(const QString& fileDir)
{
    int cnt = 0;
    int idx = fileDir.size() - 1;
    while (idx >= 0 && fileDir[idx--] != '/') cnt++;
    jointListWidget->insertItem(jointListWidget->count(), fileDir.right(cnt));
}


void SimulateControlWidget::insertObjectItem(const QString& fileDir)
{
    int cnt = 0;
    int idx = fileDir.size() - 1;
    while (idx >= 0 && fileDir[idx--] != '/') cnt++;
    objectListWidget->insertItem(objectListWidget->count(), fileDir.right(cnt));
}


void SimulateControlWidget::deleteJointItem()
{
    int row = jointListWidget->currentRow();
    jointListWidget->takeItem(row);
}

void SimulateControlWidget::deleteObjectItem()
{
    int row = objectListWidget->currentRow();
    objectListWidget->takeItem(row);
}



void SimulateControlWidget::createAction()
{

    connect(this->loadJointButton.data(), SIGNAL(clicked()), this->parent(), SLOT(loadJoint()));
    connect(this->loadObjectButton.data(), SIGNAL(clicked()), this->parent(), SLOT(loadObject()));

    connect(this->removeJointButton.data(), SIGNAL(clicked()), this->parent(), SLOT(removeJoint()));
    connect(this->removeObjectButton.data(), SIGNAL(clicked()), this->parent(), SLOT(removeObject()));


    connect(this->setRobotParameterButton.data(), SIGNAL(clicked()), this->parent(), SLOT(setRobotParameter()));
    connect(this->customObjectButton.data(), SIGNAL(clicked()), this->parent(), SLOT(customObject()));
    connect(this->assembleToolButton.data(), SIGNAL(clicked()), this->parent(), SLOT(assembleTool()));


    connect(this->hideAxis.data(), SIGNAL(toggled(bool)), this->parent(), SLOT(hideAxisMode()));


    for (int i = 0; i < jointSlider.size(); ++i) {
        connect(this->jointSlider[i].data(), SIGNAL(valueChanged(int)), jointSpinBox[i].data(), SLOT(setValue(int)));
        connect(this->jointSpinBox[i].data(), SIGNAL(valueChanged(int)), jointSlider[i].data(), SLOT(setValue(int)));
    }

    connect(this->jointSpinBox[0].data(), SIGNAL(valueChanged(int)), this->parent(), SLOT(BaseRotate(int)));
    connect(this->jointSpinBox[1].data(), SIGNAL(valueChanged(int)), this->parent(), SLOT(J1Rotate(int)));
    connect(this->jointSpinBox[2].data(), SIGNAL(valueChanged(int)), this->parent(), SLOT(J2Rotate(int)));
    connect(this->jointSpinBox[3].data(), SIGNAL(valueChanged(int)), this->parent(), SLOT(J3Rotate(int)));
    connect(this->jointSpinBox[4].data(), SIGNAL(valueChanged(int)), this->parent(), SLOT(J4Rotate(int)));
    connect(this->jointSpinBox[5].data(), SIGNAL(valueChanged(int)), this->parent(), SLOT(J5Rotate(int)));
    connect(this->jointSpinBox[6].data(), SIGNAL(valueChanged(int)), this->parent(), SLOT(J6Rotate(int)));

}






/// parameters : d1 a1 a2 a3 d4 θ1 θ2 θ3 θ4 θ5 θ6 j1 j1 j2 j2 j3 j3 j4 j4 j5 j5 j6 j6 tcfx tcfy tcfz
///      index : 0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23   24   25
void SimulateControlWidget::recvParameters(QVector<float> parameters)
{
    if (parameters.size() != 26) return;

    /// 必须先设置大小范围,否则后面设置初始化值的时候会出现超限
    for (int i = 1; i <= 6; ++i) {
        float jointMin = parameters[2*i + 9];
        float jointMax = parameters[2*i + 10];

        jointSlider[i]->setMinimum(jointMin);
        jointSlider[i]->setMaximum(jointMax);

        jointSpinBox[i]->setMinimum(jointMin);
        jointSpinBox[i]->setMaximum(jointMax);
    }

    for (int i = 1; i <= 6; ++i) {
        jointSlider[i]->setValue(parameters[4+i]);
    }

}




Joint SimulateControlWidget::getCurJointsAngle() const
{

    Joint joint;

    joint.j1 = jointSlider[1]->value();
    joint.j2 = jointSlider[2]->value();
    joint.j3 = jointSlider[3]->value();
    joint.j4 = jointSlider[4]->value();
    joint.j5 = jointSlider[5]->value();
    joint.j6 = jointSlider[6]->value();

    return joint;

}

void SimulateControlWidget::setCurJointsAngle(Joint joint)
{

    jointSlider[1]->setValue(joint.j1);
    jointSlider[2]->setValue(joint.j2);
    jointSlider[3]->setValue(joint.j3);
    jointSlider[4]->setValue(joint.j4);
    jointSlider[5]->setValue(joint.j5);
    jointSlider[6]->setValue(joint.j6);

}





