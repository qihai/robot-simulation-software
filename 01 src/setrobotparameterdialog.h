﻿#ifndef SETROBOTPARAMETERDIALOG_H
#define SETROBOTPARAMETERDIALOG_H

#include <QDialog>

namespace Ui {
class SetRobotParameterDialog;
}


class SetRobotParameterDialog : public QDialog
{
    Q_OBJECT

    public:

        SetRobotParameterDialog(QWidget *parent = 0);

        virtual ~SetRobotParameterDialog();

    signals:

        void sendParameter(QVector<float>);

    private slots:

        void on_pushButton_clicked();

        void on_pushButton_2_clicked();

    private:

        void init();

    private:

        Ui::SetRobotParameterDialog *ui;

        QVector<float> parameters;
};

#endif // SETROBOTPARAMETERDIALOG_H
