﻿#ifndef TEACHWIDGET_H
#define TEACHWIDGET_H

#include <QWidget>

namespace Ui {
class TeachWidget;
}


class TeachWidget : public QWidget
{

    Q_OBJECT

    public:

        TeachWidget(QWidget *parent = 0);

        ~TeachWidget();

        void addItem(const QString & text);

    signals:

        void moveJButton();

        void moveLineButton();

        void moveArcButton();

        void runButton();

        void clearAllButton();

        void shadowOpen();

        void shadowClose();

    private slots:

        void on_run_clicked();

        void on_moveJ_clicked();

        void on_clearAllButton_clicked();

        void on_moveLine_clicked();

        void on_checkBox_stateChanged(int arg1);

        void on_checkBox_clicked();

        void on_moveArc_clicked();

private:

        Ui::TeachWidget *ui;

};

#endif // TEACHWIDGET_H
